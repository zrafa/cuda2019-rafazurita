# cuda2019-rafazurita

Curso CÓMPUTO DE ALTO RENDIMIENTO EN GPU
========================================


Trabajo Práctico Obligatorio - curso de postgrado

Alumno: Rafael Ignacio Zurita <rafa@fi.uncoma.edu.ar>

Profesora: Dra. Mónica Denham


En este repositorio se encuentran todos los códigos fuentes solicitados en el trabajo obligatorio.

Para los ejercicios de filtros en imágenes se buscó literatura en internet por desconocer el tema. Se utilizó la siguiente referencia para la programación y confección de tales ejercicios:

```
Aplicacion de la convolucion de matrices al filtrado de imagenes
F. Gimenez-Palomares, J. A. Monsoriu, E. Alemany-Martınez - Universitat Politecnica de Valencia
ISSN 1988-3145
Modelling in Science Education and Learning Volume 9(2), 2016 doi:  10.4995/msel.2016.4524.
https://riunet.upv.es/bitstream/handle/10251/69639/4524-15767-1-PB.pdf?sequence=1
```

Uso
---

En cada carpeta  hay un Makefile

Compilar
--------

make clean;
make

Ejecutar
--------

Si nos encontramos en cder27 (nodo del cluster cder27):
make run

Algunos ejercicios aceptan argumentos de la línea de comandos. Hay que ejecutar manualmente los mismos (no con make run). Ejemplo: ejercicio 2 suma de vectores.
