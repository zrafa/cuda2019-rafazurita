/* 
 * Curso de postgrado: COMPUTO DE ALTO RENDIMIENTO EN GPU
 *
 * Filtro de Sobel
 *
 * Alumno : Rafael Ignacio Zurita <rafa@fi.uncoma.edu.ar> *
 * Profesora: Dra. Mónica Denham
 */


#include <stdio.h>
#include <stdlib.h>

#include "imagen.h"


/* ENTRADA Y SALIDA DE MAPAS */
void leer_imagen(const char * nombre, float *imagen, int filas, int cols)
{
	FILE *input = fopen(nombre,"r");

	if (input == NULL)
	{
		printf("Error en leer_imagen: no abre archivo %s \n", nombre);
		exit(-1);
	}

	int i,j, dato;

	for(i = 0; i < filas; i++)
	{
		for(j = 0; j < cols; j++)
		{
			fscanf(input, "%d", &dato);
			imagen[i*cols + j] = (float)dato;
		}

	}
	fclose(input);

}


void salvar_imagen(const char * nombre, float *imagen, int filas, int cols)
{
	FILE *output = fopen(nombre,"w");

	if (output == NULL)
	{
		printf("Error en salvar: no abre archivo %s \n", nombre);
		exit(-1);
	}

	printf("Escribiendo en archivo: %s \n", nombre);

	int i,j;
	float dato;
	for(i = 0; i < filas; i++) {
		for(j = 0; j < cols; j++) {
			dato = imagen[(filas-i)*cols + j];  // arranco desde abajo por gnuplot
			fprintf(output,"%.0f ",dato);

		}
		fprintf(output, "\n");
	}
	fclose(output);

}

/* filtro secuencial */
void filtro_sec(float *imagen_in, float *imagen_out, int filas, int cols, float *filtro)
{
	int i,j,k,l;
	float aux;

	for (i = 1; i < filas-1; i++)
		for (j = 1; j < cols-1; j++){
			aux = 0.0;

			// aplico el filtro 8 vecinos
			for(k=-1; k <= 1; k++) { // fila filtro
				for (l = -1; l <= 1; l++) {// col filtro
					aux = aux + imagen_in[(i+k)*cols + j+l] * filtro[((k+1)*3) + (l+1)];

				}
			}
			// modifico la imagen
			imagen_out[i*cols + j] = (float)aux;// / (float)9 ;

		}
}



void inicializar_filtro_sobel_horizontal(float *filtro, int tamFiltro)
{

	filtro[0] = -1.0;
	filtro[1] = 0.0;
	filtro[2] = 1.0;
	filtro[3] = -2.0;
	filtro[4] = 0.0;
	filtro[5] = 2.0;
	filtro[6] = -1.0;
	filtro[7] = 0.0;
	filtro[8] = 1.0;

}



void inicializar_filtro_sobel_vertical(float *filtro, int tamFiltro)
{
	filtro[0] = -1.0;
	filtro[1] = -2.0;
	filtro[2] = -1.0;
	filtro[3] = 0.0;
	filtro[4] = 0.0;
	filtro[5] = 0.0;
	filtro[6] = 1.0;
	filtro[7] = 2.0;
	filtro[8] = 1.0;

}


void aplicar_filtro_sobel_sec(float *imagen_in, float *imagen_out, int filas, int cols)
{
	float *filtro_hor, *filtro_ver, *g_x, *g_y;

	int dim = filas * cols * sizeof(float);

	filtro_hor = (float*)malloc(9 * sizeof(float));
	filtro_ver = (float*)malloc(9 * sizeof(float));
	g_x = (float*)malloc(dim * sizeof(float));
	g_y = (float*)malloc(dim * sizeof(float));


	if (!filtro_hor || !filtro_ver || !g_x || !g_y) {
		printf("No aloca arreglos \n ");
		exit (-1);
	}


	inicializar_filtro_sobel_vertical(filtro_ver, 9);
	inicializar_filtro_sobel_horizontal(filtro_hor, 9);

	filtro_sec(imagen_in, g_x, filas, cols, filtro_hor);

	filtro_sec(imagen_in, g_y, filas, cols, filtro_ver);

	calcular_g(g_x, g_y, imagen_out, filas, cols);


	free(filtro_hor);
	free(filtro_ver);
	free(g_x);
	free(g_y);

}


void calcular_g(float *g_x, float *g_y, float *imagen_out, int filas, int cols)
{
    int i,j;

    for (i = 0; i < filas; i++)
    	for (j = 0; j < cols; j++)
			imagen_out[i*cols + j] = (float)sqrt((float)powf(g_x[i*cols+j],2) + (float)powf(g_y[i*cols+j], 2));



}


/********************************************************************************/
/* 				PARALELO					*/
/********************************************************************************/


__global__ void kernel_aplicar_filtro(float * d_imagen_in, float * d_imagen_out, float *d_filtro, int filas, int cols)
{

   /* fila y columna del thread que define el pixel a procesar */
   int myCol = threadIdx.x + blockIdx.x * blockDim.x;
   int myRow = threadIdx.y + blockIdx.y * blockDim.y;

   /* Procesar cada pixel de la imagen. Para simplificar, no se procesan los bordes ya que el filtro utiliza los 8 vecinos */
   if ((myCol < cols-1) && (myRow < filas -1) && (myCol > 0) && (myRow > 0)) {

                int k, l, m;
                float aux = 0.0;


                /* filtro al pixel [myRow,myCol] */
                aux = 0.0;
                m = 0;  /* indice para recorrer el filtro */
                for(k=-1; k <= 1; k++) { // fila filtro
                        for (l = -1; l <= 1; l++) {// col filtro
                                aux = aux + d_imagen_in[(myRow+k)*cols + (myCol+l)] * d_filtro[m];
                                m++;
                        }
                }

                
		// modifico la imagen
                /* imagen de salida */
                d_imagen_out[myRow*cols + myCol] = (float) aux;   // al se el promedio, se debe dividir por 9
        }       


					//aux = aux + imagen_in[(i+k)*cols + j+l] * filtro[((k+1)*3) + (l+1)];
				//}
			//}
			// modifico la imagen
			//imagen_out[i*cols + j] = (float)aux;// / (float)9 ;

}


__global__ void kernel_calcular_g(float *d_g_x, float *d_g_y, float *d_imagen_out, int filas, int cols)
{

	//TODO: calcula la salida G (magnitud del gradiente)

	/* fila y columna del thread que define el pixel a procesar */
	int myCol = threadIdx.x + blockIdx.x * blockDim.x;
	int myRow = threadIdx.y + blockIdx.y * blockDim.y;

	/* Procesar cada pixel de la imagen. Para simplificar, no se procesan los bordes ya que el filtro utiliza los 8 vecinos */
	if ((myCol < cols) && (myRow < filas) && (myCol >= 0) && (myRow >= 0)) {

		//imagen_out[i*cols + j] = (float)sqrt((float)powf(g_x[i*cols+j],2) + (float)powf(g_y[i*cols+j], 2));
		d_imagen_out[myRow*cols + myCol] = (float) sqrt( (float)powf(d_g_x[myRow*cols+myCol],2) + (float)powf(d_g_y[myRow*cols+myCol], 2));
	}
	

    //int i,j;
    //for (i = 0; i < filas; i++)
    //	for (j = 0; j < cols; j++)
    //  	imagen_out[i*cols + j] = (float)sqrt((float)powf(g_x[i*cols+j],2) + (float)powf(g_y[i*cols+j], 2));

}



/*  SOBEL paralelo*/
void aplicar_filtro_sobel_par(float *d_imagen_in, float *d_imagen_out, int filas, int cols)
{

// TODO: tomando de guia la soluciona secuencial paralelice usando GPU
        float *d_filtro_hor, *d_filtro_ver, *d_g_x, *d_g_y;

        int dim = filas * cols * sizeof(float);

        cudaMalloc((void **) &d_filtro_hor, (9 * sizeof(float)));
        cudaMalloc((void **) &d_filtro_ver, (9 * sizeof(float)));
        cudaMalloc((void **) &d_g_x, (dim * sizeof(float)));
        cudaMalloc((void **) &d_g_y, (dim * sizeof(float)));

        if (!d_filtro_hor || !d_filtro_ver || !d_g_x || !d_g_y) {
                printf("No aloca arreglos \n ");
                exit (-1);
        }


	float * filtro_hor = (float*)malloc(9 * sizeof(float));
	float * filtro_ver = (float*)malloc(9 * sizeof(float));

        inicializar_filtro_sobel_horizontal(filtro_hor, 9);
        inicializar_filtro_sobel_vertical(filtro_ver, 9);

       /* TODO: copiar filtro desde host a device */
        cudaMemcpy (d_filtro_hor, filtro_hor, (9 * sizeof(float)), cudaMemcpyHostToDevice);
        cudaMemcpy (d_filtro_ver, filtro_ver, (9 * sizeof(float)), cudaMemcpyHostToDevice);


        /* lanzamiento del kernel, se crea 1 thread por pixel de la imagen en una grilla 2D que 
                 tiene el mismo tamanio que la imagen a procesar. */
        dim3 nThreads(16,16);
        dim3 nBlocks(cols/nThreads.x + (cols % nThreads.x ? 1 : 0), filas/nThreads.y + (filas % nThreads.y ? 1 : 0));

        /* lanzamiento del kernel: 1 thread por pixel de la imagen */
        kernel_aplicar_filtro<<<nBlocks, nThreads>>>(d_imagen_in, d_g_x, d_filtro_hor, filas, cols);
        cudaDeviceSynchronize();

        kernel_aplicar_filtro<<<nBlocks, nThreads>>>(d_imagen_in, d_g_y, d_filtro_ver, filas, cols);
        cudaDeviceSynchronize();

        kernel_calcular_g<<<nBlocks, nThreads>>>(d_g_x, d_g_y, d_imagen_out, filas, cols);
        cudaDeviceSynchronize();

        //filtro_sec(imagen_in, g_x, filas, cols, filtro_hor);
        //filtro_sec(imagen_in, g_y, filas, cols, filtro_ver);
        //calcular_g(g_x, g_y, imagen_out, filas, cols);


        cudaFree(d_g_x);
        cudaFree(d_g_y);

        cudaFree(d_filtro_hor);
        cudaFree(d_filtro_ver);

        free(filtro_hor);
        free(filtro_ver);


}
