
/* 
 * Curso de postgrado: COMPUTO DE ALTO RENDIMIENTO EN GPU
 *
 * Suma de Matrices
 *
 * Alumno : Rafael Ignacio Zurita <rafa@fi.uncoma.edu.ar> *
 * Profesora: Dra. Mónica Denham
 */

#include <cuda_runtime.h>
#include <stdio.h>
#include "gpu_timer.h"
#include "cpu_timer.h"

/*
Suma de matrices
 */


int FILAS;
int COLS;

void inicilizar_Matriz(float *data, const int size)
{
    int i;

    for(i = 0; i < size; i++)
    {
        data[i] = (float)(rand() / 10.0f);
    }

    return;
}

void sum_matrix_sec(float *A, float *B, float *C, const int filas, const int cols)
{
	/* TODO: resolver la suma de matrices secuencial */
	int i, j;
	for (i=0; i<filas; i++)
		for (j=0; j<cols; j++)
			C[i*cols + j] = A[i*cols + j] + B[i*cols + j];

}


void checkResult(float *hostRef, float *gpuRef, const int N)
{
    double epsilon = 1.0E-8;
    bool match = 1;

    for (int i = 0; i < N; i++)
    {
        if (abs(hostRef[i] - gpuRef[i]) > epsilon)
        {
            match = 0;
            printf("host %f gpu %f %d \n", hostRef[i], gpuRef[i], i);
            break;
        }
    }

    if (match)
        printf("Test pasado! \n\n");
    else
        printf("Test no pasado! \n\n");
}



// grid 2D block 2D
__global__ void sum_matrix_par(float *MatA, float *MatB, float *MatC, int cols, int filas)
{
    /* TODO: estudiar el indexado usando f y c */
    int c = threadIdx.x + blockIdx.x * blockDim.x;
    int f = threadIdx.y + blockIdx.y * blockDim.y;
    /* TODO: completar y asignar en idx el indice del thread */
    int idx = f * cols + c;

    /* TODO: resuelva la suma de matrices */
    if (c < cols && f < filas)  // por qué es necesario este control?
                                /* Respuesta: porque tenemos mas threads (multimplo de 32) que 
                                 * los necesarios para el tamanio de las matrices?
                                 */
        MatC[idx] = MatA[idx] + MatB[idx];
}



void uso_y_salir() {

        printf("\nUso:\n\n");
        printf("./suma_matrices FILAS COLUMNAS\n\n");

        exit(EXIT_FAILURE);
}


int main( int argc, char *argv[] ) {

        /* obligamos a que se ingresen los dos argumentos: FILAS y COLUMNAS */
        if (argc < 3)
                uso_y_salir();

        FILAS = atoi(argv[1]);
        COLS = atoi(argv[2]);


    // datos de la placa
    int dev = 0;
    cudaDeviceProp deviceProp;
    cudaGetDeviceProperties(&deviceProp, dev);
    printf("Se usa placa %d: %s\n", dev, deviceProp.name);
    cudaSetDevice(dev);


    int nBytes = FILAS * COLS * sizeof(float);
    printf("Tamanio de Matrix : %d x  %d \n", FILAS, COLS);


    /* TODO: aloque memoria en host para las matrices h_A y h_B y las matrices resultados hostRef y gpuRef*/
      // malloc host memory
    float *h_A, *h_B, *hostRef, *gpuRef;
    h_A = (float *) malloc(nBytes);
    h_B = (float *) malloc(nBytes);
    hostRef = (float *) malloc(nBytes);
    gpuRef = (float *) malloc(nBytes);


    // inicilizacion de matrice en host
    cpu_timer crono_cpu;
    crono_cpu.tic();
    inicilizar_Matriz(h_A, FILAS*COLS);
    inicilizar_Matriz(h_B, FILAS*COLS);
    crono_cpu.tac();
    printf("Inicializacion de matrices en host  %f msec\n", crono_cpu.elapsed());

    /* Inicializacion de las matrices resultado en 0*/
    memset(hostRef, 0, nBytes);
    memset(gpuRef, 0, nBytes);


    // add matrix at host side for result checks
    crono_cpu.tic();
    sum_matrix_sec(h_A, h_B, hostRef, FILAS, COLS);
    crono_cpu.tac();

    printf("Suma de matrices en host  %lf msec\n", crono_cpu.elapsed());


    /* TODO: aloque memoria en device para las matrices d_MatA, d_MatB y d_MatbC */
    // malloc device global memory
    float *d_MatA, *d_MatB, *d_MatC;
    cudaMalloc((void **) &d_MatA, nBytes);
    cudaMalloc((void **) &d_MatB, nBytes);
    cudaMalloc((void **) &d_MatC, nBytes);


    /* TODO: complete la transferencia de memoria de las matrices inicializadas desde host a device  (h_A -> d_MatA y h_B -> d_MatB) */
    // transfer data from host to device
    cudaMemcpy (d_MatA, h_A, nBytes, cudaMemcpyHostToDevice);
    cudaMemcpy (d_MatB, h_B, nBytes, cudaMemcpyHostToDevice);


    /* TODO: complete para armar una grilla de bloques de 32x32 threads, con los bloques que hagan falta para solucionar el problema */
    /* por ejemplo: */
    int dimx = 32;
    int dimy = 32;
    dim3 block(dimx, dimy);
    // sacar los 1s y completar como corresponda
    // Muy grande de prueba dim3 grid (1000, 1000);

    // viejo : dim3 grid ( (FILAS/dimx) + 1, (COLS/dimy) + 1);
    /* ejemplo: 5000 filas / 32 + 1 = 157,  157*157 = (aprox) 25200000. y 5000*5000=25000000 */

    dim3 grid (COLS/dimx + (COLS%dimx?1:0),FILAS/dimy + (FILAS % dimy?1:0));



    /* Lanzamiento del kernel al kernel */
    gpu_timer crono_gpu;
    crono_gpu.tic();

    sum_matrix_par<<<grid, block>>>(d_MatA, d_MatB, d_MatC, COLS, FILAS);
    cudaDeviceSynchronize();

    crono_gpu.tac();
    printf("Suma de matrices en GPU  %lf msec\n", crono_gpu.ms_elapsed);


    cudaGetLastError();

    // copy kernel result back to host side
    cudaMemcpy(gpuRef, d_MatC, nBytes, cudaMemcpyDeviceToHost);

    /* Muestro en pantalla algunos resultados*/
    /* ya no es nescesario
    int i, j;
    for (i=0; i<3; i++)
        for (j=0; j<3; j++) {
		printf("i=%i, j=%i, h_A[x][y]=%f ", i, j, h_A[i*COLS + j]);
		printf("i=%i, j=%i, h_B[x][y]=%f ", i, j, h_B[i*COLS + j]);
		printf("i=%i, j=%i, hostRef[x][y]=%f ", i, j, hostRef[i*COLS + j]);
		printf("i=%i, j=%i, gpuRef[x][y]=%f ", i, j, gpuRef[i*COLS + j]);
		printf("\n");
	};
    */



    // check device results
    checkResult(hostRef, gpuRef, FILAS*COLS);

    /* TODO: desaloque memoria de device */
    cudaFree(d_MatA);
    cudaFree(d_MatB);
    cudaFree(d_MatC);

    /* TODO: desaloque memoria de host */
    free(h_A);
    free(h_B);
    free(hostRef);
    free(gpuRef);

    return (0);
}
