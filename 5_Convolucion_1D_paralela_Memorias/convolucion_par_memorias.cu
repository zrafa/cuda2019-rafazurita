/* 
 * Curso de postgrado: COMPUTO DE ALTO RENDIMIENTO EN GPU
 *
 * Convolucion paralela : memoria compartida y memoria constante
 *
 * Alumno : Rafael Ignacio Zurita <rafa@fi.uncoma.edu.ar> *
 * Profesora: Dra. Mónica Denham
 */

#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <cmath>
#include <time.h>
#include <cuda_runtime_api.h>

#include "gpu_timer.h"
#include "cpu_timer.h"



/* Tamanio del array de input */
int N = 512*3500; 

/* Tamanio del filtro - debe ser menor igual a 1024*/
const int M = 64;  

/* Floating point type */
typedef float FLOAT;
// typedef double FLOAT;


/* Funcion para preparar el filtro */
void SetupFilter(FLOAT* filter, size_t size) 
{
	/* TODO: llene el filtro con el valor que desee*/
	for(int i = 0; i < size; i++){
		/* utilizamos el ejemplo visto en la clase */
		filter[i] = 1.0/size;
	}
}


/* Convolucion en la cpu */
void conv_cpu(const FLOAT* input, FLOAT* output, const FLOAT* filter) 
{
	/* TODO: implemente la versión secuencial de la convolucion */
	FLOAT temp;

	/*Barrido vector input (tamaño N) y para cada elemento j hasta N hago la operacion*/
	/*de convolucion: elemento i del vector filter por elemento i+j del vector input */

	int i, j;
        for (j=0; j<N; j++) {
                temp = 0.0;
                for (i=0; i<M; i++) {
                        temp += filter[i]*input[i+j];
                }
                output[j] = temp;
        }

}


// declaracino del filtro en memoria constante
__constant__ FLOAT d_filtro_constant[M];

__global__ void conv_gpu_constant_memory (const FLOAT* input, FLOAT* output) 
{	  	
	  int j = blockIdx.x * blockDim.x + threadIdx.x;

	  /* TODO: resuelva la convolucion utilizando el filtro en memoria constante */
   
          output[j] = 0;
          for (int i = 0; i < M; i++) {
                  output[j] += d_filtro_constant[i]*input[i+j];
          }       

}


// convolucion usando indexado unidimensional de threads/blocks
// un thread por cada elemento del output todo en memoria global
__global__ void conv_gpu (const FLOAT* input, FLOAT* output, const FLOAT* filter) 
{	  	
	int j = blockIdx.x * blockDim.x + threadIdx.x;
	/*Barro vector input (tamaño N) y para cada elemento j hasta N hago la operacion*/
	/*de convolucion: elemento i del vector filter por elemento i+j del vector input */

	/*TODO: descomente y complete para resolver la convolucion paralela*/
        output[j] = 0;
        for (int i = 0; i < M; i++){
                output[j] += filter[i]*input[i+j];
        }       

}



/* Solucion que solo sirve para Nh menor a tamaño de bloque */
__global__ void conv_gpu_shared_memory(const FLOAT *input, FLOAT *output, const FLOAT *filter)
{

	int tidx = blockIdx.x * blockDim.x + threadIdx.x; // global
	int id = threadIdx.x;

	__shared__ float filter_sm[M];
	
	/* TODO: lleno el vector de memoria compartida con los datos del filtro*/
	if (id < M)
		filter_sm[id] = filter[id];

	// todos los threads del bloque se deben sincronizar antes de seguir	
	__syncthreads();


	/*Barro vector input (tamaño N) y para cada elemento j hasta N hago la operacion*/
	/*de convolucion: elemento i del vector filter por elemento i+j del vector input*/
	/* TODO: realicela convolucion usando el filtro en memoria compartida */	
	int i;
	output[tidx]=0.0;
	for (i=0; i<M; i++) {
	  	output[tidx] += filter_sm[i]*input[i+tidx];
	}	

}

void uso_y_salir() {

        printf("\nUso:\n\n");
        printf("./convolucion N \n\n");
        printf("N: tamanio de la entrada. M es una constante en el código: tamanio del filtro.\n");
        printf("M <= 1024 y M divisor de N. Ejemplo: N=1792000, M=1024.\n\n");

        exit(EXIT_FAILURE);
}


int main( int argc, char *argv[] ) {

        /* obligamos a que se ingrese el argumento N */
        if (argc < 2)
                uso_y_salir();

        N = atoi(argv[1]);

	/* Imprime input/output general */
	printf("Input size N: %d\n", N);
	printf("Filter size M: %d\n", M);

	/* se imprime el nobre de la placa */
	int card;
	cudaGetDevice(&card);
    	cudaDeviceProp deviceProp;
   	 cudaGetDeviceProperties(&deviceProp, card);
	printf("\nDevice %d: \"%s\" \n", card, deviceProp.name);

	/* chequeos de dimensiones de senial y filtro */
	assert(N % M == 0);
	assert(M <= 1024);


	/* TODO: aloque memoria en host para el input, output, check output y filtro  */
	FLOAT *h_input, *h_output, *check_output, *h_filter;
	h_input = (FLOAT *) malloc((N + M) * sizeof(FLOAT));
	h_output = (FLOAT *) malloc(N * sizeof(FLOAT));
	check_output = (FLOAT *) malloc(N * sizeof(FLOAT));
	h_filter = (FLOAT *) malloc(M * sizeof(FLOAT));

	/* Inicializa el filtro */
	SetupFilter(h_filter, M);

	/* Llena el array de input (CON "padding") con numeros aleatorios acotados */
	for(int i = 0 ; i < N+M ; i++){
		h_input[i] = (FLOAT)(rand() % 100); 
	}

	/* TODO: alocar memoria en device para el input, filtro, y los outputs de las 3 versiones en GPU */
	FLOAT *d_input, *d_output, *d_filter, *d_output_sm, *d_output_cm;
	cudaMalloc((void **) &d_input, ((N + M) * sizeof(FLOAT)));  		// senial
	cudaMalloc((void **) &d_filter, M * sizeof(FLOAT));  	// filtro
	cudaMalloc((void **) &d_output, N * sizeof(FLOAT));   	// salida convolucion paralela normal
	cudaMalloc((void **) &d_output_sm, N * sizeof(FLOAT));  	// salida usando memoria compartida
	cudaMalloc((void **) &d_output_cm, N * sizeof(FLOAT));  	// salida usando memoria de constantes
    
	// pongo a cero el device output
	cudaMemset(d_output,0,N * sizeof(FLOAT));
	cudaMemset(d_output_sm,0,N * sizeof(FLOAT));
	cudaMemset(d_output_cm,0,N * sizeof(FLOAT));

	/* TODO: copiar senial de entrada (h_input) y filtro en GPU*/
        cudaMemcpy(d_input, h_input, (N + M) * sizeof(FLOAT), cudaMemcpyHostToDevice);
        cudaMemcpy(d_filter, h_filter, M * sizeof(FLOAT), cudaMemcpyHostToDevice);

	
	/* cronometraje */
	cpu_timer crono_cpu; 
	crono_cpu.tic();

	/* convolucion en la CPU */
	conv_cpu(h_input, check_output, h_filter);

	crono_cpu.tac();

	/****************************************************/
	/* VERSION PARALELA  -  FILTRO EN MEMORIA GLOBAL    */
	/****************************************************/

  	/*Defino tamaño bloque y grilla */
  	dim3 block_size(M);
  	dim3 grid_size(N/M);

  	gpu_timer crono_gpu;
  	crono_gpu.tic();
    
        /* TODO: lanzamiento del kernel que usa memoria global para filtro y senial. Salida queda en d_output */
	conv_gpu<<< grid_size, block_size>>>(d_input, d_output, d_filter);

	crono_gpu.tac();
	
	/* TODO: copiar el resultado de device a host usando h_output y d_output */
	cudaMemcpy(h_output, d_output, N * sizeof(FLOAT), cudaMemcpyDeviceToHost);

	/* Comparacion (lea documentacion de la funcion de C assert si no la conoce)*/	
	for (int j=0; j<N; j++) {
		assert(h_output[j] == check_output[j]);
	}



	/*****************************************************/
	/* VERSION PARALELA  -  FILTRO EN MEMORIA COMPARTIDA */
	/*****************************************************/

	gpu_timer crono_gpu_sm;
  	crono_gpu_sm.tic();
   
   /* TODO: lanzamiento del kernel que usa memoria compartida para filtro. Salida queda en d_output_sm */
	conv_gpu_shared_memory<<< grid_size , block_size >>>(d_input, d_output_sm, d_filter);

	crono_gpu_sm.tac();

	/* TODO: copiar el resultado de device a host usando h_output y d_output_sm */
	cudaMemcpy(h_output, d_output_sm, N * sizeof(FLOAT), cudaMemcpyDeviceToHost);
	

	/* Comparacion (lea documentacion de la funcion de C assert si no la conoce)*/
	for(int j=0; j<N; j++){
		assert(h_output[j] == check_output[j]);
	}


	/****************************************************/
	/* VERSION PARALELA  -  MEMORIA DE CONSTANTES 		*/
	/****************************************************/

	/* TODO: copiar h_filter en d_filtro constante */
	cudaMemcpyToSymbol(d_filtro_constant, h_filter, M * sizeof(FLOAT));

	gpu_timer crono_gpu_cm;
  	crono_gpu_cm.tic();
   
   /* TODO: lanzamiento del kernel que usa memoria de constantes para filtro. Salida queda en d_output_cm */
	conv_gpu_constant_memory<<< grid_size , block_size >>>(d_input, d_output_cm);

	crono_gpu_cm.tac();


	/* TODO: copiar el resultado de device a host usando h_output y d_output_cm */
	cudaMemcpy(h_output, d_output_cm, N * sizeof(FLOAT), cudaMemcpyDeviceToHost);


	/* Comparacion (lea documentacion de la funcion de C assert si no la conoce)*/
	for(int j=0; j<N; j++){
		assert(h_output[j] == check_output[j]);
	}

	
	
/* Impresion de tiempos */
	printf("[N/M/ms_cpu/ms_gpu/ms_gpu_sm/ms_gpu_cm]= [%d/%d/%lf/%lf/%lf/%lf] \n", N, M, crono_cpu.ms_elapsed, crono_gpu.ms_elapsed, crono_gpu_sm.ms_elapsed, crono_gpu_cm.ms_elapsed);
   

	/* TODO: liberer memoria en host y device */
	free(h_input);
	free(h_output);
	free(check_output);
	free(h_filter);
	

	cudaFree(d_input);
	cudaFree(d_filter);
	cudaFree(d_output);
	cudaFree(d_output_sm);
	cudaFree(d_output_cm);
}

